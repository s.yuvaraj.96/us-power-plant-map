import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopNPowerplantsComponent } from './top-n-powerplants.component';

describe('TopNPowerplantsComponent', () => {
  let component: TopNPowerplantsComponent;
  let fixture: ComponentFixture<TopNPowerplantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopNPowerplantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopNPowerplantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
