import { Component, OnInit } from '@angular/core';
import *  as Highcharts from "highcharts/highmaps";
import { StateWiseViewService } from "../../service/state-wise-view.service";
import { HttpParams } from '@angular/common/http';
import { StateDropDown } from 'src/app/models/state.dropdown.model';
import proj4 from 'proj4';
import usAllData from '../../../assets/us-map/us-all.topo.json';

@Component({
  selector: 'app-state-wise-view',
  templateUrl: './state-wise-view.component.html',
  styleUrls: ['./state-wise-view.component.css']
})
export class StateWiseViewComponent implements OnInit {

  Highcharts: typeof Highcharts = Highcharts;
  chartStateWiseOptions: Highcharts.Options = {};
  chartPlantWiseOptions: Highcharts.Options = {};
  chartConstructor = "mapChart";
  states: StateDropDown[] = [];
  isStateWiseOptionSelected: boolean = true;
  selectedValue: StateDropDown = new StateDropDown();


  constructor(private stateWiseViewService: StateWiseViewService) {
    // to initialize the chart data at first time
    this.buildUSChartData([], []);
    this.buildUSStateChartData({}, [], []);

  }

  ngOnInit(): void {

    this.loadStates();
    this.Highcharts.setOptions({ lang: { thousandsSep: ',' } });
    this.buildUSChart();

  }

  /**
   * This method is used to build US Chart
   */
  private buildUSChart() {

      const data = Highcharts.geojson(usAllData);
      const mapView = usAllData.objects.default['hc-recommended-mapview'];

      this.stateWiseViewService.getStateWiseNetPower().subscribe((statesData) => {
        const resultMap = statesData.reduce(function (map: any, obj: any) {
          map[obj.code] = obj;
          return map;
        }, {});
        // set data
        data.forEach((d, i) => {
          const stateData = resultMap[d.properties['postal-code']];
          if (stateData) {
            d.value = stateData['percentage'];
            d.properties['state-data'] = stateData;
          }
        });
        this.buildUSChartData(data, mapView);

      });

  }

  /**
   * This method is used to build the us chart data
   * @param data        map data
   * @param mapView     map view
   */
  private buildUSChartData(data: any, mapView: any) {
    this.chartStateWiseOptions = {
      chart: {
        borderWidth: 2,
      },

      title: {
        text: 'US State Wise Net Power Generation'
      },

      colorAxis: {
        min: 0,
        minColor: '#E6E7E8',
        maxColor: '#005645'
        // maxColor: '#530c56'
      },

      mapView: mapView,

      mapNavigation: {
        enabled: true,
        buttonOptions: {
          verticalAlign: 'bottom'
        }
      },

      plotOptions: {
        map: {
          states: {
            hover: {
              color: '#EEDD66'
            }
          }
        }
      },

      series: [{
        data,
        name: 'USA',
        dataLabels: {
          enabled: true,
          format: '{point.properties.postal-code}'
        },
        custom: {
          mapView
        },
        type: 'map',
        tooltip: {
          pointFormatter: function () {
            const stateName: string = this.properties['state-data']['name'];
            const totalPower: number = this.properties['state-data']['totalPower'];
            const totalPowerStr: string = totalPower.toLocaleString('en-US', { maximumFractionDigits: 3 });
            const percentage = this.properties['state-data']['percentage'];
            return `<b>State Name: ${stateName}</b></br><b>Total Net Power Generated: ${totalPowerStr} MWh</b></br><b>Percentage: ${percentage}%</b>`;
          }
        }
      }]
    }
  }


  /**
   * This method is used to build US state chart
   * @param state     state data
   */
  private buildUSStateChart(state: any) {
    const mapKey = `countries/us/us-${state.code.toLowerCase()}-all`;

    this.stateWiseViewService.getUsMapStateTopJsonDetails(mapKey).subscribe((topology) => {
      const params: HttpParams = new HttpParams().set('stateCode', state.code);
      this.stateWiseViewService.getPowerPlantsByState(params).subscribe((powerplantData) => {
        const powerPlantMapData = this.buildPowerPlantData(powerplantData);
        const stateMapData = [{ 'hc-key': `us-${state?.code.toLowerCase()}` }];
        console.log(Highcharts.geojson(usAllData));
        this.buildUSStateChartData(state, Highcharts.geojson(topology), powerPlantMapData, stateMapData);
      });
    });
  }

  /**
   * This method is used to build us state chart data
   * @param state         selected state
   * @param topology      map data
   * @param data          data
   * @param stateMapData  state map data
   */
  private buildUSStateChartData(state?: any, topology?: any, data?: any, stateMapData?: any) {
    this.chartPlantWiseOptions = {
      chart: {
        borderWidth: 2,
        proj4: proj4,
      },
      title: {
        text: `${state.value} Power Plants Map`
      },

      tooltip: {
        pointFormatter: function () {
          const powerPlantName: string = this.name;
          // @ts-ignore
          const totalPower: number = this.totalPower;
          const totalPowerStr: string = totalPower.toLocaleString('en-US', { maximumFractionDigits: 3 });
          const percentage = this.percentage;
          return `<b>Power Plant Name: ${powerPlantName}</b></br><b>Total Net Power Generated: ${totalPowerStr} MWh</b></br><b>Percentage: ${percentage}%</b>`;
        }
      },
      plotOptions: {
        series: {
          turboThreshold: 3000
        }
      },
      series: [{
        allAreas: false,
        data: stateMapData,
        showInLegend: false,
        enableMouseTracking: false,
        type: 'map',
        mapData: Highcharts.geojson(usAllData),
        joinBy: 'hc-key',
        states: {
          hover: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false,
          format: '{point.name}'
        }
      },
      {
        type: 'mappoint',
        dataLabels: {
          enabled: true,
          format: '{point.capital}'
        },
        name: 'Power Plants',
        data: data,
        color: 'black'
      }]
    };

  }

  /**
   * This method is used to load state drop down data
   */
  private loadStates() {
    this.stateWiseViewService.getAllStates().subscribe((statesData: any) => {
      const stateDropDownData: StateDropDown[] = [];
      statesData.forEach((state: any) => {
        const stateDropDown = new StateDropDown();
        stateDropDown.code = state.code;
        stateDropDown.value = state.name;
        stateDropDownData.push(stateDropDown);
      });

      this.states = stateDropDownData;
      this.selectedValue = stateDropDownData[0];
      this.buildUSStateChart(this.selectedValue);
    });
  }

  /**
   * This method is used to handle state selection
   * @param $event      event data
   */
  handleStateSelection($event: any) {
    this.buildUSStateChart($event);
  }

  /**
   * This method is used to build power plant data
   * @param powerplantData      powerplant data
   */
  private buildPowerPlantData(powerplantData: any) {
    return powerplantData.map((powerPlant: any) => {
      return {
        name: powerPlant.name,
        lon: powerPlant?.location.coordinates[0],
        lat: powerPlant?.location.coordinates[1],
        percentage: powerPlant.percentage,
        totalPower: powerPlant.totalPower
      };
    });
  }
}
