import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  links = ['Top N PowerPlants', 'State Wise View'];
  activeLink = this.links[0];

}
