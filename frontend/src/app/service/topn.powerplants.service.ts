import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApiReferenceService } from './api-reference.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TopNPowerPlantsService {

  constructor(private http: HttpClient, private apiRefernceService: ApiReferenceService) { }


  getTopNPowerPlants(params: any): Observable<any> {
    return this.http.get(this.apiRefernceService.getTopNPowerPlants, { params: params });
  }


}
