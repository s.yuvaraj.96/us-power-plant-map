import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiReferenceService {

  baseUrl = environment.baseUrl;

  getTopNPowerPlants = this.baseUrl + '/api/power-plants/top';

  getStatesNetPower = this.baseUrl + '/api/states/net-power';

  getPowerPlantsByState = this.baseUrl + '/api/states/power-plants/top';

  getAllStates = this.baseUrl + '/api/states';

}
