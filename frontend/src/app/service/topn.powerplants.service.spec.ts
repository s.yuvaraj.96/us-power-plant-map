import { TestBed } from '@angular/core/testing';

import { Topn.PowerplantsService } from './topn.powerplants.service';

describe('Topn.PowerplantsService', () => {
  let service: Topn.PowerplantsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Topn.PowerplantsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
