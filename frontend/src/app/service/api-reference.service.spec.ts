import { TestBed } from '@angular/core/testing';

import { ApiReferenceService } from './api-reference.service';

describe('ApiReferenceService', () => {
  let service: ApiReferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiReferenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
