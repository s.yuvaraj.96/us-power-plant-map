import { TestBed } from '@angular/core/testing';

import { StateWiseViewService } from './state-wise-view.service';

describe('StateWiseViewService', () => {
  let service: StateWiseViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StateWiseViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
