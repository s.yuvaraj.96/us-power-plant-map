import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { ApiReferenceService } from "./api-reference.service";

@Injectable({
  providedIn: 'root'
})
export class StateWiseViewService {


  constructor(private http: HttpClient, private apiRefernceService: ApiReferenceService) { }

  getStateWiseNetPower(): Observable<any> {
    return this.http.get(this.apiRefernceService.getStatesNetPower);
  }

  getPowerPlantsByState(params: HttpParams): Observable<any> {
    return this.http.get(this.apiRefernceService.getPowerPlantsByState,  { params: params });
  }


  getAllStates(params?: HttpParams): Observable<any> {
    return this.http.get(this.apiRefernceService.getAllStates,  { params: params });
  }


  getUsMapAllTopJsonDetails(): Observable<any> {
    return this.http.get('https://code.highcharts.com/mapdata/countries/us/us-all.topo.json');
  }

  getUsMapStateTopJsonDetails(mapKey: string): Observable<any> {
    const url = `https://code.highcharts.com/mapdata/${mapKey}.topo.json`;
    return this.http.get(url);
  }
}
