import {Injectable} from '@angular/core';
import {
    HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse
} from '@angular/common/http';

import {Observable, throwError } from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';


/** Pass untouched request through to the next request handler. */
@Injectable({
    providedIn: 'root'
})
export class Interceptor implements HttpInterceptor {
    public status: any;
    private pendingRequests = 0;

    constructor(private ngxLoader: NgxUiLoaderService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<any> {
        this.pendingRequests++;
        this.ngxLoader.start();
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                return throwError(error);
            })).pipe(
            finalize(() => {
                this.pendingRequests--;
                if (this.pendingRequests === 0) {
                    this.ngxLoader.stop();
                }
            })
        );
    }
}
