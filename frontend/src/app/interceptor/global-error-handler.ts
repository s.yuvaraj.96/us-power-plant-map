import { ErrorHandler, Injectable } from "@angular/core";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private ngxLoader: NgxUiLoaderService) {}
  handleError(error: any) {
    this.ngxLoader.stop();
    console.error('Error from global error handler', error);
  }
}
