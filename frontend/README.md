# US PowerPlant Map

Application to visualize US Power plant Data in Map and Charts using HighCharts

## Development server

To start the development express server, run the following

```bash
npm run start
```

To build the project , run the following

```bash
npm run build
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Use Docker
You can also run this app as a Docker container:

Step 1: Clone the repo us-power-plant-map

Step 2: Build the Docker image

```bash
docker build -t  . <image_name>
```

Step 3: Run the Docker container locally:

```bash
docker run -p 80:80 -d <image_name>
```

#End Results

 Please check following for end results
 
 - Top N power plants display in Charts
 
    ![alt text](../sample-results/US_TopN_in_Charts.png)
 
  - Top N power plants display in Maps
  
    ![alt text](../sample-results/US_TopN_in_Maps.png)
  
 - State wise data US Map 
 
    ![alt text](../sample-results/US_State_Wise_Map.png)
 
 - Filtered State Power Plant data Map 
 
    ![alt text](../sample-results/US_State_Wise_Power_Plant.png)
