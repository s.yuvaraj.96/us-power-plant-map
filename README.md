
## The US Power Plant Map Application

This Application is used to get the statistical data of US power generation by power plants

## Project Details

* backend - API application developed using express js
* frontend - Angular Application with HighCharts for map visualization

#Project API Details

- Exposed APIs to GET/PUT/POST/DELETE states data

- Exposed APIs to GET/PUT/POST/DELETE power plant data

- Exposed APIS to GET top N, State wise and plant wise data

- Visualize Map using Angular and HighCharts

#End Results

 Please check following for end results
 
 - Top N power plants display in Charts
 
    ![alt text](/sample-results/US_TopN_in_Charts.png)
 
  - Top N power plants display in Maps
  
    ![alt text](/sample-results/US_TopN_in_Maps.png)
  
 - State wise data US Map 
 
    ![alt text](/sample-results/US_State_Wise_Map.png)
 
 - Filtered State Power Plant data Map 
 
    ![alt text](/sample-results/US_State_Wise_Power_Plant.png)
 
## Assumptions
 - Always receive excel file with specific format
 - Power Plant Sheet will have location data
 - Power plant code will remain constant

## Cloud Deployment
 
   - We can convert express application into serverless lambda then can be deployed in any cloud platform(Refer backend [README.md](/backend/README.md))
   - With AWS cloud we can use Amplify to deploy both Backend and Front end App
   - S3 can also be used to host the angular application
    

## Future Enhancements
   - Visualize year wise generation 
   - Compare Chart with previous years
   - Can show Generator Details in Power Plants(ex: No of generators in the plant, fuel type, generator online, generator retirement year)
   - Chart to show sub region based generation
   - Search using Power plant Name
   - Search using Location
   
