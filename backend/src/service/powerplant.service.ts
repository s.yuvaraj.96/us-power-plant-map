import { Request } from "express";
import logger from "../utils/logger";
import { v1 as uuidv1 } from 'uuid';
import PowerPlant from "../models/powerplant.model";

/**
 * This is used to save the power plant details in DB
 * @param data      - power plant data
 * @returns         - saved power plant data
 */
const createPowerPlant = async (data: any) => {
    const powerPlantByCode = await PowerPlant.findOne({ code: data.code }).exec();
    if (powerPlantByCode)
        throw new Error('Power Plant already exists');
    try {
        const powerPlant = new PowerPlant({
            plantId: uuidv1(),
            name: data.name,
            code: data.code,
            status: 'Active',
            loc: { type: 'Point', coordinates: [data.loc.lng, data.loc.lat] },
            stateCode: data.stateCode,
            year: data.year,
            annualNetPowerGen: data.genntan,
            annualOzonePowerGen: data.genntoz
        });
        return await PowerPlant.create(powerPlant);
    } catch (e) {
        logger.error({ e }, 'Unable to save Power Plant');
        throw new Error('Unable to save Power Plant');
    }
};

/**
 * This is used to update the power plant details
 * @param plantId        - plant id
 * @param data           - update plant data
 */
const updatePowerPlant = async (plantId: any, data: any) => {
    try {
        return await PowerPlant.findOneAndUpdate(
            { plantId: plantId },
            {
                $set: {
                    name: data.name,
                    code: data.code,
                    status: data.status,
                    loc: { type: 'Point', coordinates: [data.loc.lng, data.loc.lat] },
                    stateCode: data.stateCode,
                    year: data.year,
                    annualNetPowerGen: data.genntan,
                    annualOzonePowerGen: data.genntoz
                }
            }
        ).exec();
    } catch (e) {
        logger.error({ e }, 'Unable to update Power Plant');
        throw new Error('Unable to update Power Plant');
    }
};

/**
 * This is used to delete the power plant detailss in DB
 * @param plantId       - plant id
 * @returns             - deleted result
 */
const deletePowerPlant = async (plantId: any) => {
    try {
        return await PowerPlant.findOneAndDelete({ plantId: plantId.toString() }).exec();
    } catch (e) {
        logger.error({ e }, 'Unable to delete Power Plant');
        throw new Error('Unable to delete Power Plant');
    }
};

/**
 * This is used to delete all the power plant details
 * @returns     - returns delete response
 */
const deleteAllPowerPlant = async () => {
    try {
        return await PowerPlant.deleteMany().exec();
    } catch (e) {
        logger.error({ e }, 'Unable to delete Power Plant');
        throw new Error('Unable to delete Power Plant');
    }
};

/**
 * This is used to find all active power plants
 * @param req       - request object
 * @returns         - all matched results
 */
const findAllActivePowerPlants = async (req: Request) => {
    try {
        const name = req.query['name'];
        const code = req.query['code'];
        let filterCriteria: any = { status: 'Active' };
        if (name) {
            filterCriteria['name'] = { $regex: new RegExp(name.toString().toLowerCase()), $options: 'i' };
        }
        if (code) {
            filterCriteria['code'] = code;
        }
        return await PowerPlant.find(filterCriteria, { _id: 0, __v: 0 }).sort("name").exec();
    } catch (e) {
        logger.error({ e }, 'Unable to find Power Plant');
        throw new Error('Unable to find Power Plant');
    }
};

/**
 * This is used to find power plant by plant id
 * @param plantId       - plant id
 * @returns             - matched power plant data
 */
const findPowerPlantByPlantId = async (plantId: string) => {
    try {
        return await PowerPlant.findOne({ plantId: plantId }, { _id: 0, __v: 0 }).exec();
    } catch (e) {
        logger.error({ e }, 'Unable to find Power Plant for the given plant id');
        throw new Error('Unable to find Power Plant for the given plant id');
    }
};

/**
 * This is used to find power plant by code
 * @param code      - power plant code
 */
const findPowerPlantByCode = async (code: string) => {
    try {
        return await PowerPlant.findOne({ code: code }, { _id: 0, __v: 0 }).exec();
    } catch (e) {
        logger.error({ e }, 'Unable to find Power Plant for the given plant code');
        throw new Error('Unable to find Power Plant for the given plant code');
    }
};

/**
 * This is used to find the top generated plants in whole US ( plant net generation / sum of net power generation of all plants in US  * 100)
 * @param limit     - limit
 * @param year      - year of generation
 * @returns         - top power plants
 */
const findTopNPowerPlants = async (limit: number, year: number = 2020) => {
    const totalNetPowerForYear: any = await findTotalNetPowerGeneratedForYear(year);
    return PowerPlant.aggregate()
        .match({
            year: year
        }).group({
            _id: { code: "$code" },
            totalPowerByPlant: { "$sum": { $abs: { $add: ["$annualNetPowerGen", "$annualOzonePowerGen"] } } },
            name: { $first: "$name" },
            location: { $first: "$loc" },
        }).project({
            _id: 0,
            code: "$_id.code",
            name: 1,
            location: 1,
            totalPower: "$totalPowerByPlant",
            percentage: {
                $round: [{ "$multiply": [{ "$divide": ["$totalPowerByPlant", totalNetPowerForYear[0].totalNetPower] }, 100] }, 2]
            }
        }).sort({
            percentage: -1
        }).limit(limit)
        .allowDiskUse(true)
        .exec();
}

/**
 * This is used to find states net power generation of federal state ( sum of all plant net generation of state / sum of all plants in US power generation * 100)
 * @param limit         - limit of records
 * @param year          - year of generation
 * @returns             - state net power data 
 */
const findStatesNetPowerWithPercentage = async (limit?: number, year: number = 2020) => {
    const totalNetPowerForYear: any = await findTotalNetPowerGeneratedForYear(year);
    const aggregareQuery = PowerPlant.aggregate()
        .match({
            year: year,
            status: "Active"
        }).group({
            _id: { stateCode: "$stateCode" },
            totalPowerByState: { "$sum": { $abs: { $add: ["$annualNetPowerGen", "$annualOzonePowerGen"] } } },
        }).lookup({
            from: "states",
            localField: "_id.stateCode",
            foreignField: "code",
            as: "state"
        }).unwind({ path: "$state" })
        .project({
            _id: 0,
            code: "$_id.stateCode",
            name: "$state.name",
            location: 1,
            totalPower: "$totalPowerByState",
            percentage: {
                $round: [{ "$multiply": [{ "$divide": ["$totalPowerByState", totalNetPowerForYear[0].totalNetPower] }, 100] }, 2]
            }
        }).allowDiskUse(true)

    aggregareQuery.sort({ percentage: -1 });
    if (limit) {
        aggregareQuery.limit(limit);
    }

    return aggregareQuery.exec();
}

/**
 * This is used to find the total net power generated of all plants for year
 * @param year      - year of generation
 * @returns         - sum of all plants generation
 */
const findTotalNetPowerGeneratedForYear = async (year: number = 2020) => {
    const totalNetPowerForYear: any = PowerPlant.aggregate()
        .match({
            year: year,
            status: "Active"
        }).group({
            _id: { year: "$year" },
            totalNetPower: { "$sum": { $abs: { $add: ["$annualNetPowerGen", "$annualOzonePowerGen"] } } }
        }).project({
            totalNetPower: 1
        }).allowDiskUse(true)
        .exec()

    return totalNetPowerForYear;
}

/**
 * This is used to finf the total net power generation by state
 * @param stateCode         - state code
 * @param year              - year
 * @returns                 - returns sum of generation of all plants in the given state 
 */
const findTotalNetPowerGeneratedByState = async (stateCode: string, year: number = 2020) => {
    const totalNetPowerByState: any = PowerPlant.aggregate()
        .match({
            year: year,
            stateCode: stateCode,
            status: "Active"
        }).group({
            _id: { stateCode: "$stateCode" },
            totalNetPower: { "$sum": { $abs: { $add: ["$annualNetPowerGen", "$annualOzonePowerGen"] } } }
        }).project({
            totalNetPower: 1
        }).allowDiskUse(true)
        .exec()

    return totalNetPowerByState;
}

/**
 * This is used to find top power plants in specific state ( plant net power generation / sum of all plant net power in that state * 100)
 * @param stateCode 
 * @param limit 
 * @param year 
 * @returns 
 */
const findTopPowerPlantsWithinState = async (stateCode: string, limit?: number, year: number = 2020) => {
    const totalNetPowerByState: any = await findTotalNetPowerGeneratedByState(stateCode, year);
    const totalNetPower = totalNetPowerByState[0]?.totalNetPower || 0;
    const aggregareQuery = PowerPlant.aggregate()
        .match({
            year: year,
            status: "Active",
            stateCode: stateCode
        }).group({
            _id: { code: "$code" },
            totalPowerByPlant: { "$sum": { $abs: { $add: ["$annualNetPowerGen", "$annualOzonePowerGen"] } } },
            name: { $first: "$name" },
            location: { $first: "$loc" },
        }).project({
            _id: 0,
            code: "$_id.code",
            name: 1,
            location: 1,
            totalPower: "$totalPowerByPlant",
            percentage: {
                $round: [{ "$multiply": [{ "$divide": ["$totalPowerByPlant",  totalNetPower]}, 100] }, 2]
            }
        }).allowDiskUse(true)

    aggregareQuery.sort({ percentage: -1 });
    if (limit) {
        aggregareQuery.limit(limit);
    }

    return aggregareQuery.exec();
}

/**
 * This is used to create power plants as bulk insert
 * @param powerPlantsData   - array of power plants data
 * @returns                 - insert response
 */
const createManyPowerPlant = async (powerPlantsData: any) => {
    try {
        const powerPlants = [];
        for (const data of powerPlantsData) {
            const powerPlant = new PowerPlant({
                plantId: uuidv1(),
                name: data.name,
                code: data.code,
                status: 'Active',
                loc: { type: 'Point', coordinates: [data.loc.lng, data.loc.lat] },
                stateCode: data.stateCode,
                year: data.year,
                annualNetPowerGen: data.genntan,
                annualOzonePowerGen: data.genntoz
            });
            powerPlants.push(powerPlant);
        }
        return await PowerPlant.insertMany(powerPlants);
    } catch (e) {
        logger.error({ e }, 'Unable to save Power Plant');
        throw new Error('Unable to save Power Plant');
    }
};



export { createPowerPlant, createManyPowerPlant, updatePowerPlant, deletePowerPlant, deleteAllPowerPlant, findPowerPlantByPlantId, findAllActivePowerPlants, findPowerPlantByCode, findTopNPowerPlants, findStatesNetPowerWithPercentage, findTopPowerPlantsWithinState }