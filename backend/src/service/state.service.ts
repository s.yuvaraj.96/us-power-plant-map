import { Request } from "express";
import State from "../models/state.model";
import logger from "../utils/logger";
import { v1 as uuidv1 } from 'uuid';

/**
 * This is used to save the state in Database
 * @param data          - state data
 * @returns             - saved data
 */
const createState = async (data: any) => {
    const stateByCode = await State.findOne({ code: data.code }).exec();
    if(stateByCode)
       throw new Error('State already exists');
    try {
        const state = new State({
            stateId: uuidv1(),
            name: data.name,
            code: data.code,
            status: 'Active'
        });
        return await State.create(state);
    } catch (e) {
        logger.error({e}, 'Unable to save state');
        throw new Error('Unable to save state');
    }
};

/**
 * This is used to update the state in database by state id
 * @param stateId       - state id
 * @param data          - data to be updated
 * @returns             - updated data
 */
const updateState = async (stateId: any, data: any) => {
    try {

        return await State.findOneAndUpdate(
            { stateId: stateId },
            {
                $set: {
                    name: data.name,
                    code: data.code,
                    status: data.status
                }
            }
        ).exec();
    } catch (e) {
        logger.error({e}, 'Unable to update the state');
        throw new Error('Unable to update the state');
    }
};

/**
 * This is used to delete the state from the database by state id
 * @param stateId       - state id
 * @returns             - success response code
 */
const deleteState = async (stateId: any) => {
    try {
        return await State.findOneAndDelete({ stateId: stateId.toString() }).exec();
    } catch (e) {
        logger.error({e}, 'Unable to delete the state');
        throw new Error('Unable to delete the state');
    }
};

/**
 * This is used to delete all the state data from database
 * @returns     - uccess response code 
 */
const deleteAllState = async () => {
    try {
        return await State.deleteMany({}).exec();
    } catch (e) {
        logger.error({e}, 'Unable to delete the state');
        throw new Error('Unable to delete the state');
    }
};

/**
 * This is used to find all the active states in DB
 * @param req       - request object
 * @returns         - matched all active states data
 */
const findAllActiveStates = async (req: Request) => {
    try {
        const name = req.query['name'];
        const code = req.query['code'];
        let filterCriteria: any = { status: 'Active' };
        if(name) {
            filterCriteria['name'] = { $regex: new RegExp(name.toString().toLowerCase()), $options: 'i'};
        }
        if(code) {
            filterCriteria['code'] = code;
        }
        return await State.find(filterCriteria, {_id: 0, __v: 0} ).sort("name").exec();
    } catch (e) {
        logger.error({e}, "Unable to find active states");
        throw new Error("Unable to find active states");
    }
};

/**
 * This is used to find the state by state id
 * @param stateId       - state id
 * @returns             - delete results 
 */
const findStateByStateId = async (stateId: string) => {
    try {
        return await State.findOne({ stateId: stateId }, {_id: 0, __v: 0}).exec();
    } catch (e) {
        logger.error({e}, "Unable to find the state by id");
        throw new Error("Unable to find the state by id");
    }
};

/**
 * This is used to find the states by state code
 * @param code      - state code
 * @returns         - return matched state data
 */
const findStateByCode = async (code: string) => {
    try {
        return await State.findOne({ code: code }, {__v: 0}).exec();
    } catch (e) {
        logger.error({e}, "Unable to find state by code");
        throw new Error("Unable to find state by code");
    }
};



export { createState, updateState, deleteState, deleteAllState, findStateByStateId, findAllActiveStates, findStateByCode}