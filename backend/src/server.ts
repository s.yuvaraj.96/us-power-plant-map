import connectDatabase from "./utils/connectdb";
import logger from "./utils/logger";
import app from "./app";
import dotenv from "dotenv";
import config from "config";
dotenv.config();

const port = config.get<number>("port");

app.listen(port, async () => {

    await connectDatabase();

    logger.info(`App is running at http://localhost:${port}`);

});
