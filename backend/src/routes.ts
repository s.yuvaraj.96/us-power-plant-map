import express from "express";
import {getHealthHandler} from "./controller/health.controller";
import {loadDataInToDBHandler} from "./controller/load.data.controller";
import {
    createPowerPlantHandler,
    deletePowerPlantHandler,
    findAllPowerPlantsHandler,
    findPowerPlantByPlantIdHandler,
    findTopNPowerPlantsHandler,
    findTopPowerPlantsWithinStateHandler,
    updatePowerPlantHandler
} from "./controller/powerplant.controller";
import {
    createStateHandler,
    deleteStateHandler,
    findAllStatesHandler,
    findStateByStateIdHandler,
    updateStateHandler,
    findStatesNetPowerWithPercentageHandler
} from "./controller/state.controller";
import validateResource from "./middleware/validateResource";
import {
    createPowerPlantSchema,
    deletePowerPlantSchema,
    getPowerPlantSchema,
    updatePowerPlantSchema
} from "./schema/powerplant.schema";
import {createStateSchema, deleteStateSchema, getStateSchema, updateStateSchema} from "./schema/state.schema";

const router = express.Router();

router.get("/health", getHealthHandler);

router.post("/api/load-data", loadDataInToDBHandler);

router.get("/api/power-plants/top", findTopNPowerPlantsHandler);

router.get("/api/states/net-power", findStatesNetPowerWithPercentageHandler);

router.get("/api/states/power-plants/top", findTopPowerPlantsWithinStateHandler);


router.post("/api/states", validateResource(createStateSchema), createStateHandler);

router.get("/api/states", findAllStatesHandler);

router.put("/api/states/:stateId", validateResource(updateStateSchema), updateStateHandler);

router.delete("/api/states/:stateId", validateResource(deleteStateSchema), deleteStateHandler);

router.get("/api/states/:stateId", validateResource(getStateSchema), findStateByStateIdHandler);


router.post("/api/power-plants", validateResource(createPowerPlantSchema), createPowerPlantHandler);

router.get("/api/power-plants", findAllPowerPlantsHandler);

router.put("/api/power-plants/:plantId", validateResource(updatePowerPlantSchema), updatePowerPlantHandler);

router.delete("/api/power-plants/:plantId", validateResource(deletePowerPlantSchema), deletePowerPlantHandler);

router.get("/api/power-plants/:plantId", validateResource(getPowerPlantSchema), findPowerPlantByPlantIdHandler);



export default router;
