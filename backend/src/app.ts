import express from "express";
import cors from "cors";
import errorHandler from "./utils/errorhandler";
import appRoutes from "./routes"

const app = express();

app.use(express.json());

app.use(cors());

app.use(errorHandler);

app.use("/", appRoutes);

export default app;
