import { Errback, NextFunction, Request, Response } from "express";
import logger from "../utils/logger";


const errorHandler = (err: Errback, req: Request, res: Response, next: NextFunction) => {
    logger.error({err}, "Error Occurred");
    res.status(500);
    res.status(500).send({ message: 'Something went wrong!' , code: 500});
};


 export default errorHandler;
