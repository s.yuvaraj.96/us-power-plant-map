import { nativeEnum, number, object, string, TypeOf } from "zod";
import { StatusEnum } from "../enums/status.enum";

const latlngRegex = new RegExp("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}");

const createPayload = {
    body: object({
        name: string({
            required_error: "Plant Name is required",
        }).min(2, "Name should be at least 2 characters long")
            .max(100, "Name should be at most 100 characters long"),
        code: string({
            required_error: "Plant Code is required",
        }).min(1, "Code should be at least 1 characters long")
            .max(100, "Code should be at most 100 characters long"),
        stateCode: string({
            required_error: "State code is required",
        }),
        loc: object({

            lat: string({
                required_error: "latitude of the plant is required",
            }).regex(latlngRegex, "Invalid lattitude"),

            lng: string({
                required_error: "longitude of the plant is required",
            }).regex(latlngRegex, "Invalid longitude")
        }),
        year: number ({
            required_error: "year of the power generation is required",
        })
    }),
};

const updatePayload = {
    body: object({
        name: string({
            required_error: "Plant Name is required",
        }).min(2, "Name should be at least 2 characters long")
            .max(100, "Name should be at most 100 characters long"),
        code: string({
            required_error: "Plant Code is required",
        }).min(2, "Code should be at least 2 characters long")
            .max(50, "Name should be at most 50 characters long"),
        stateCode: string({
            required_error: "State code is required",
        }),
        loc: object({

            lat: string({
                required_error: "latitude of the plant is required",
            }).regex(latlngRegex, "Invalid lattitude"),

            lng: string({
                required_error: "longitude of the plant is required",
            }).regex(latlngRegex, "Invalid longitude")  
        }),
        status: nativeEnum(StatusEnum)
    }),
};

const params = {
    params: object({
        plantId: string({
            required_error: "power plantId is required",
        }),
    }),
};

const createPowerPlantSchema = object({
    ...createPayload,
});

const updatePowerPlantSchema = object({
    ...updatePayload,
    ...params,
});

const deletePowerPlantSchema = object({
    ...params,
});

const getPowerPlantSchema = object({
    ...params,
});

type CreatePowerPlantInput = TypeOf<typeof createPowerPlantSchema>;
type UpdatePowerPlantInput = TypeOf<typeof updatePowerPlantSchema>;
type ReadPowerPlantInput = TypeOf<typeof getPowerPlantSchema>;
type DeletePowerPlantInput = TypeOf<typeof deletePowerPlantSchema>;


export { createPowerPlantSchema, updatePowerPlantSchema, deletePowerPlantSchema, getPowerPlantSchema };
export { CreatePowerPlantInput, UpdatePowerPlantInput, ReadPowerPlantInput, DeletePowerPlantInput };