import { object, string, TypeOf, nativeEnum } from "zod";
import { StatusEnum } from "../enums/status.enum";
import _ from "lodash";

const createPayload = {
    body: object({
        name: string({
            required_error: "Name is required",
        }).min(2, "Name should be at least 2 characters long")
            .max(100, "Name should be at most 100 characters long"),
        code: string({
            required_error: "Code is required",
        }).min(2, "Code should be at least 2 characters long")
            .max(5, "Name should be at most 5 characters long")
    }),
};


const updatePayload = {
    body: object({
        name: string({
            required_error: "Name is required",
        }).min(2, "Name should be at least 2 characters long")
            .max(100, "Name should be at most 100 characters long"),
        code: string({
            required_error: "Code is required",
        }).min(2, "Code should be at least 2 characters long")
            .max(5, "Name should be at most 5 characters long"),
       status: nativeEnum(StatusEnum)      
    }),
};

const params = {
    params: object({
        stateId: string({
            required_error: "stateId is required",
        }).uuid({ message: "Invalid UUID" }),
    }),
};

const createStateSchema = object({
    ...createPayload,
});

const updateStateSchema = object({
    ...updatePayload,
    ...params,
});

const deleteStateSchema = object({
    ...params,
});

const getStateSchema = object({
    ...params,
});

type CreateStateInput = TypeOf<typeof createStateSchema>;
type UpdateStateInput = TypeOf<typeof updateStateSchema>;
type ReadStateInput = TypeOf<typeof getStateSchema>;
type DeleteStateInput = TypeOf<typeof deleteStateSchema>;


export { createStateSchema, updateStateSchema, deleteStateSchema, getStateSchema };
export { CreateStateInput, UpdateStateInput, ReadStateInput, DeleteStateInput };