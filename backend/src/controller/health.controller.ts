import { Request, Response } from "express";

/**
 * This handler is used to return health status
 * @param req   - request object
 * @param res   - respons object
 */
export async function getHealthHandler(req: Request, res: Response) {
    const data = {
        uptime: process.uptime(),
        message: 'Ok',
        date: new Date()
    };
    res.status(200).send(data);
}
