import { Request, Response } from "express";
import _ from "lodash";
import { CreatePowerPlantInput, DeletePowerPlantInput, ReadPowerPlantInput, UpdatePowerPlantInput } from "../schema/powerplant.schema";
import { createPowerPlant, deletePowerPlant, findAllActivePowerPlants, findPowerPlantByPlantId, findTopNPowerPlants, findTopPowerPlantsWithinState, updatePowerPlant } from "../service/powerplant.service";

/**
 * This handler is used to create the powerplant
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns created power plant
 */
const createPowerPlantHandler = async (req: Request<{}, {}, CreatePowerPlantInput["body"]>, res: Response) => {

    const body = req.body;

    const state = await createPowerPlant({ ...body }); 

    return res.send(state).sendStatus(201);
};

/**
 * This handler is used to update the powerplant
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns updated powerplant
 */
const updatePowerPlantHandler = async (req: Request<UpdatePowerPlantInput["params"]>, res: Response) => {

    const plantId = req.params.plantId.toString();
    const update = req.body;

    const powerPlant = await findPowerPlantByPlantId(plantId);

    if (!powerPlant) {
        return res.status(404).send({ message: "Given power plant not found" });
    }

    const updatedPowerPlant = await updatePowerPlant({ plantId }, update);

    return res.send(updatedPowerPlant);
};

/**
 * This handler is used to delete the powerplant
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns http status 200
 */
const deletePowerPlantHandler = async (req: Request<DeletePowerPlantInput["params"]>, res: Response) => {
 
    const plantId = req.params.plantId.toString();

    const powerPlant = await findPowerPlantByPlantId(plantId);

    if (!powerPlant) {
        return res.status(404).send({ message: "Given power plant not found" });
    }

    await deletePowerPlant(plantId);

    return res.sendStatus(200);
};

/**
 * This handler is used to find the powerplant by powerplant id
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns matched powerplant
 */
const findPowerPlantByPlantIdHandler = async (req: Request<ReadPowerPlantInput["params"]>, res: Response) => {

    const plantId = req.params.plantId.toString();

    const powerPlant = await findPowerPlantByPlantId(plantId);

    if (!powerPlant) {
        return res.status(404).send({ message: "Given power plant not found" });
    }

    return res.send(powerPlant);
};

/**
 * This handler is used to find all the powerplants
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns all powerplants
 */
const findAllPowerPlantsHandler = async (req: Request, res: Response) => {

    const powerPlants = await findAllActivePowerPlants(req);

    return res.send(powerPlants);
};

/**
 * This handler is used to get top n power plants
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns top N powerplants
 */
const findTopNPowerPlantsHandler = async (req: Request, res: Response) => {
    const limit: number = parseInt(req.query.limit?.toString() || '5');
    const year: number = parseInt(req.query.year?.toString() || '2020');

    const powerPlants = await findTopNPowerPlants(limit, year);

    return res.send(powerPlants);

};

/**
 * This handler is used to get top n power plants with in states
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns top N powerplants with in states
 */
 const findTopPowerPlantsWithinStateHandler = async (req: Request, res: Response) => {
    const year: number = parseInt(req.query.year?.toString() || '2020');
    const stateCode = req.query.stateCode?.toString() || '';
    if(!stateCode) {
        res.status(400).json({ message: "State Code is required"});
    }
    let limit: any = undefined;
    if(req.query.limit) {
        limit = parseInt(req.query.limit?.toString());
    }
    const powerPlants = await findTopPowerPlantsWithinState(stateCode, limit, year);

    return res.send(powerPlants);

};


export { createPowerPlantHandler, updatePowerPlantHandler, deletePowerPlantHandler, findPowerPlantByPlantIdHandler , findAllPowerPlantsHandler, findTopNPowerPlantsHandler, findTopPowerPlantsWithinStateHandler };

