import { Request, Response } from "express";
import { CreateStateInput, DeleteStateInput, ReadStateInput, UpdateStateInput } from "../schema/state.schema";
import { findStatesNetPowerWithPercentage } from "../service/powerplant.service";
import { createState, deleteState, findAllActiveStates, findStateByStateId, updateState } from "../service/state.service";

/**
 * This handler is used to create the state
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns created state
 */
const createStateHandler = async (req: Request<{}, {}, CreateStateInput["body"]>, res: Response) => {

    const body = req.body;

    const state = await createState({ ...body }); 

    return res.send(state).sendStatus(201);
};

/**
 * This handler is used to update the state by state id
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns updated state
 */
const updateStateHandler = async (req: Request<UpdateStateInput["params"]>, res: Response) => {

    const stateId = req.params.stateId.toString();
    const update = req.body;

    const state = await findStateByStateId(stateId);

    if (!state) {
        return res.status(404).send("Given state not found");
    }

    const updatedState = await updateState({ stateId }, update);

    return res.send(updatedState);
};

/**
 * This handler is used to delete the state by state id
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns success status
 */
const deleteStateHandler = async (req: Request<DeleteStateInput["params"]>, res: Response) => {
 
    const stateId = req.params.stateId.toString();

    const state = await findStateByStateId(stateId);

    if (!state) {
        return res.status(404).send({ message: "Given state not found"});
    }

    await deleteState(stateId);

    return res.sendStatus(200);
};

/**
 * This handler is used to find te state by state id
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns matched state
 */
const findStateByStateIdHandler = async (req: Request<ReadStateInput["params"]>, res: Response) => {

    const stateId = req.params.stateId.toString();

    const state = await findStateByStateId(stateId);

    if (!state) {
        return res.status(404).send({ message: "Given state not found"});
    }

    return res.send(state);
};

/**
 * This handler is used to find all the states
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns all states
 */
const findAllStatesHandler = async (req: Request, res: Response) => {

    const states = await findAllActiveStates(req);

    return res.send(states);
};

/**
 * To find the net power generation of states with percentage
 * @param req   - request object
 * @param res   - response object
 * @returns     - result containing 
 */
const findStatesNetPowerWithPercentageHandler = async (req: Request, res: Response) => {
    const year: number = parseInt(req.query.year?.toString() || '2020');
    
    let limit: any = undefined;
    if(req.query.limit) {
        limit = parseInt(req.query.limit?.toString())
    }

    const statesNetPowerWithPercentage = await findStatesNetPowerWithPercentage(limit, year);

    return res.send(statesNetPowerWithPercentage);

};

export { createStateHandler, updateStateHandler, deleteStateHandler, findStateByStateIdHandler, findAllStatesHandler, findStatesNetPowerWithPercentageHandler }
