import { Request, Response } from "express";
import fs from "fs";
import excelToJson from "convert-excel-to-json";
import _, { isNaN } from "lodash";
import { createState, deleteAllState, findStateByCode, updateState } from "../service/state.service";
import logger from "../utils/logger";
import { createManyPowerPlant, createPowerPlant, deleteAllPowerPlant, findPowerPlantByCode, updatePowerPlant } from "../service/powerplant.service";

/**
 * This handler is used to load the initial data into mongo DB
 * @param req   - request object
 * @param res   - response object
 * @returns     - returns success message
 */
const loadDataInToDBHandler = async (req: Request, res: Response) => {

    await loadStates();
    await loadPowerPlants();
    res.json({message: 'Data loaded successfully'});
};


/**
 * To Load the states from json file
 */
const loadStates = async () => {
    const statesDataRaw = fs.readFileSync('data/states.json');
    const statesData = JSON.parse(statesDataRaw.toString());

    try {
        await deleteAllState();
    } catch (e) {
        logger.error({ e }, "Error while deleting state")
    }


    for (const newState of statesData) {
        try {
            await createState(newState);
        } catch (e) {
            logger.error({ e }, "Error while saving state")
        }
    }
    logger.info("States loaded succesfully");
};

/**
 * To Load the power plant details from excel and save in Mongo DB
 */
const loadPowerPlants = async () => {

    logger.info("started loading power plant details")
    const plantDetails = loadDataFromExcel();

    const plantLatLngDetails = plantDetails['PLNT20'];
    const plantGenerationDetails = plantDetails['GEN20'];

    // grouping by same power plant name and state code
    const groupedGenerationMap = _.groupBy(plantGenerationDetails, (powerplant) => powerplant['plantCode']);
    const plantLatLngMap = _.keyBy(plantLatLngDetails, (powerplant) => powerplant['plantCode']);

    const keys = Object.keys(groupedGenerationMap);
    const powerPlantDetails: any = [];
    const locationNotFoundPlants: any = [];
    keys.forEach(key => {
        const plantGenDetails = groupedGenerationMap[key];
        let genntan = 0;
        let genntoz = 0;
        plantGenDetails.forEach(plantGenSetData => {
            const currentGenntan = plantGenSetData['genntan'];
            genntan += _.isNil(currentGenntan) || _.isNaN(currentGenntan) ? 0 : currentGenntan;

            const currentGenntoz = plantGenSetData['genntoz'];
            genntoz += _.isNil(currentGenntoz) || _.isNaN(currentGenntoz) ? 0 : currentGenntoz;
        });


        const stateCode = plantGenDetails[0].stateCode;
        const plantName = plantGenDetails[0].plantName;
        const plantCode = plantGenDetails[0].plantCode;
        const year = plantGenDetails[0].year;
        const plantLocationDetails: any = plantLatLngMap[key];
        if (plantLocationDetails && !_.isNil(plantLocationDetails['lat']) && !_.isNil(plantLocationDetails['lng'])) {
            const loc = { lat: plantLocationDetails['lat'], lng: plantLocationDetails['lng'] };
            const powerPlantDetail = { name: plantName, code: plantCode, loc: loc, stateCode: stateCode, year: year, genntan: genntan, genntoz: genntoz }
            powerPlantDetails.push(powerPlantDetail);
        } else {
            locationNotFoundPlants.push(`"${plantName} + ${plantCode}"`);
        }
    });


    logger.info("Location not found for plants", locationNotFoundPlants);
    
    await savePowerPlantDetails(powerPlantDetails);
    logger.info("Power Plant Data loaded successfully into DB");
}

/**
 * To Save the power plant details
 * @param powerPlantDetails     - power plant details to be saved
 */
const savePowerPlantDetails = async (powerPlantDetails: any) => {
    try {
        await deleteAllPowerPlant();
        // create all at once
        await createManyPowerPlant(powerPlantDetails);
    }
    catch (e) {
        logger.error({ e }, "Error while creating power plant")
    }

};

/**
 * Loads the data from the excel
 * @returns     - returns data from the excel
 */
const loadDataFromExcel = () => {
    return excelToJson({
        sourceFile: 'data/egrid2020_data.xlsx',
        sheets: [{
            name: 'PLNT20',
            columnToKey: {
                C: 'stateCode',
                D: 'plantName',
                E: 'plantCode',
                T: 'lat',
                U: 'lng'
            },
            header: {
                rows: 2
            },
        }, {
            name: 'GEN20',
            columnToKey: {
                B: 'year',
                C: 'stateCode',
                D: 'plantName',
                E: 'plantCode',
                M: 'genntan',
                N: 'genntoz'
            },
            header: {
                rows: 2
            },
        }]
    });
}


export { loadDataInToDBHandler }
