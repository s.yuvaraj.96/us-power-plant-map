import mongoose, { Schema } from "mongoose";
import { StatusEnum } from "../enums/status.enum"

const stateSchema = new Schema({
    stateId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 100
    },
    code: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 5
    },
    status: {
        type: String,
        required: true,
        enum: Object.values(StatusEnum)
    }
}, { timestamps: true });

// Indexing for search
stateSchema.index({ "name": "text" });

// indexing for business unique
stateSchema.index({ "code": 1 }, { unique: true });
// indexing for schema unique
stateSchema.index({ "stateId": 1 }, { unique: true });


const State = mongoose.model('State', stateSchema);

export default State;