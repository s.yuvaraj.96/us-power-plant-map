import mongoose, { Schema } from "mongoose";

const powerPlantSchema = new Schema({
    plantId: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 100
    },
    code: {
        type: String,
        required: true,
        minLength: 1,
        maxLength: 100
    },
    loc: {
        type: { type: String },
        coordinates: []
    },
    stateCode: {
        type: String
    },
    status: {
        type: String,
        required: true,
        enum: ['Active', 'InActive']
    },
    annualNetPowerGen: {
        type: Number,
        default: 0
    },
    annualOzonePowerGen: {
        type: Number,
        default: 0
    },
    year: {
        type: Number,
        required: true
    }

}, { timestamps: true });



// define for location
powerPlantSchema.index({ loc: '2dsphere' });

// indexing for business unique
powerPlantSchema.index({ "code": 1 }, { unique: true });
// indexing for schema unique
powerPlantSchema.index({ "plantId": 1 }, { unique: true });

// indexing for search
powerPlantSchema.index({ "name": "text" });

powerPlantSchema.index({ "stateCode": 1});


const PowerPlant = mongoose.model('PowerPlant', powerPlantSchema);

export default PowerPlant;

