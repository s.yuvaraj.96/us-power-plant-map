import app from '../src/app';
import request from 'supertest';
import State from "../src/models/state.model";
import mongoose from "mongoose";



describe('All States APIs /api/power-plants/', () => {

    beforeEach(function(done: any) {
        if (mongoose.connection.db) return done();
        mongoose.connect('mongodb://admin:!d!al!r%40123@localhost:27017/us-power-plant?authSource=admin', done);
    });

    afterEach(async () => {
        await State.deleteMany({});
    });

    describe('GET All api/states', () => {

        beforeEach(async () => {
            await State.collection.insertMany([{ "name" : "Wyoming", "code" : "WY"}, { 	"name" : "Virgin Islands", "code" : "VI" }]);
        });

        afterAll(async () => {
            await State.deleteMany({});
        });

        it('should return all available states', async () => {

            const res = await request(app).get('/api/states');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some((g: any) => g.code === 'WY')).toBeTruthy();
            expect(res.body.some((g: any) => g.code === 'VI')).toBeTruthy();
        });

        it('should return a state if a valid name is passed', async () => {
            const res = await request(app).get('/api/states?name=Wyoming');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(1);
            expect(res.body).toHaveProperty('name', 'Wyoming');
            expect(res.body).toHaveProperty('code', 'WY');
        });

        it('should return a state if a valid code is passed', async () => {
            const res = await request(app).get('/api/states?code=VI');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(1);
            expect(res.body).toHaveProperty('name', 'Virgin Islands');
            expect(res.body).toHaveProperty('code', 'VI');
        });
    });

    describe('POST /api/states', () => {

        afterEach(async () => {
            await State.deleteMany({});
        });


        it('should return 400 if code has less than 2 characters', async () => {
            await request(app)
                .post("/api/states")
                .send({ "name" : "Wyoming", "code" : "W"})
                .expect(400);
        });

        it('should return 400 if code has greater than 5 characters', async () => {
            await request(app)
                .post("/api/states")
                .send({ "name" : "Wyoming", "code" : "WKKKKKK"})
                .expect(400);
        });

        it('should return 400 if name has less than 2 characters', async () => {
            await request(app)
                .post("/api/states")
                .send({ "name" : "w", "code" : "WY"})
                .expect(400);
        });

        it('should return 400 if name has greater than 100 characters', async () => {
            await request(app)
                .post("/api/states")
                .send({ "name" : "rgbHumohvHDwSROUDNgHTrgMRUtHHmWoUHNpVttsLBcYObASeRSwaqXzPwZHktOlrKhbirWwplSMuQWudjSlolXVpLADImpiREdR", "code" : "WY"})
                .expect(400);
        });

        it('should create a new state if valid input is passed', async () => {
            const state = await request(app)
                .post("/api/states")
                .send({ "name" : "Wyoming", "code" : "WY"});
            expect(state).not.toBeNull();
        });

        it('should return 400 if duplicate input passed', async () => {
            await State.collection.insertMany([{ "name" : "Wyoming", "code" : "WY"}, { 	"name" : "Virgin Islands", "code" : "VI" }]);
            await request(app)
                .post("/api/states")
                .send({ "name" : "Wyoming", "code" : "WY"})
                .expect(400);
        });

    });

    describe('DELETE State by State Id', () => {

        afterEach(async () => {
            await State.deleteMany({});
        });

        const execDelete = (stateId: any) => {
            return request(app).delete('/api/states/' + stateId);
        };


        it('should return 404 if state id is not valid', async () => {
            const stateId = 'e7fd8862-a77d-46d6-945e-747eb8ee7a27';
            const res = await execDelete(stateId);
            expect(res.status).toBe(404);
        });

        it('should delete a state if valid input is passed', async () => {
            const state: any = await request(app).post("/api/states").send({ "name" : "test1", "code" : "WY1"});
            await execDelete(state.stateId).expect(200);
        });
    });

    describe('GET State by State Id', () => {

        afterEach(async () => {
            await State.deleteMany({});
        });

        const execGet = (stateId: any) => {
            return request(app).get('/api/states/' + stateId);
        };

        it('should return 404 if state id is not valid', async () => {
            const stateId = 'e7fd8862-a77d-46d6-945e-747eb8ee7a27';
            const res = await execGet(stateId);
            expect(res.status).toBe(404);
        });

        it('should get a state if valid input is passed', async () => {
            const state: any = await request(app).post("/api/states").send({ "name" : "test", "code" : "WY"});
            await execGet(state.stateId).expect(200);
        });
    });
});
