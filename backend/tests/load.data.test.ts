import app from '../src/app';
import request from 'supertest';
import mongoose from "mongoose";



describe("POST /api/load-data", () => {

    beforeEach(function(done: any) {
        if (mongoose.connection.db) return done();
        mongoose.connect('mongodb://admin:!d!al!r%40123@localhost:27017/us-power-plant?authSource=admin', done);
    });

    jest.setTimeout(10000000);
    it('should load the data', async () => {
        await request(app).post("/api/load-data").expect(200);
    });

    it('should have the data', async () => {
        const stateData = await request(app).get("/api/states");
        expect(stateData).toBeTruthy();
    });
});
