import app from '../src/app';
import request from 'supertest';
import mongoose from "mongoose";
import PowerPlant from "../src/models/powerplant.model";



describe('All Power Plants APIs /api/power-plants/', () => {

    beforeEach(function(done: any) {
        if (mongoose.connection.db) return done();
        mongoose.connect('mongodb://admin:!d!al!r%40123@localhost:27017/us-power-plant?authSource=admin', done);
    });

    afterEach(async () => {
        await PowerPlant.deleteMany({});
    });

    describe('GET All api/powerplants', () => {

        beforeEach(async () => {
            const powerPlantDetail1 = { name: '0 Hammond St', code: '64876898677665', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            const powerPlantDetail2 = { name: '1 Hammond St', code: '648768986776678', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            await PowerPlant.collection.insertMany([powerPlantDetail1, powerPlantDetail2]);
        });

        afterAll(async () => {
            await PowerPlant.deleteMany({});
        });

        it('should return all available power plants', async () => {

            const res = await request(app).get('/api/power-plants');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some((g: any) => g.code === '64876898677665')).toBeTruthy();
            expect(res.body.some((g: any) => g.code === '648768986776678')).toBeTruthy();
        });

        it('should return a power plants if a valid name is passed', async () => {
            const res = await request(app).get('/api/power-plants?name=0');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(1);
            expect(res.body).toHaveProperty('name', '0 Hammond St');
            expect(res.body).toHaveProperty('code', '64876898677665');
        });

        it('should return a power plant if a valid code is passed', async () => {
            const res = await request(app).get('/api/power-plants?code=64876898677665');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(1);
            expect(res.body).toHaveProperty('name', '0 Hammond St');
            expect(res.body).toHaveProperty('code', '64876898677665');
        });
    });

    describe('POST /api/power-plants', () => {

        afterEach(async () => {
            await PowerPlant.deleteMany({});
        });


        it('should return 400 if code has less than 2 characters', async () => {
            const powerPlantDetail = { name: '1 Hammond St', code: '6', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            await request(app)
                .post("/api/power-plants")
                .send(powerPlantDetail)
                .expect(400);
        });

        it('should return 400 if code has greater than 50 characters', async () => {
            const powerPlantDetail = { name: '1 Hammond St', code: '66785469436964986438909863357777777779644444444444444333333333333333333333333333', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            await request(app)
                .post("/api/power-plants")
                .send(powerPlantDetail)
                .expect(400);
        });

        it('should return 400 if name has less than 2 characters', async () => {
            const powerPlantDetail = { name: 'H', code: '678655', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            await request(app)
                .post("/api/power-plants")
                .send(powerPlantDetail)
                .expect(400);
        });

        it('should return 400 if name has greater than 100 characters', async () => {
            const powerPlantDetail = { name: 'rgbHumohvHDwSROUDNgHTrgMRUtHHmWoUHNpVttsLBcYObASeRSwaqXzPwZHktOlrKhbirWwplSMuQWudjSlolXVpLADImpiREdR', code: '678655', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            await request(app)
                .post("/api/power-plants")
                .send(powerPlantDetail)
                .expect(400);
        });

        it('should create a new power plant if valid input is passed', async () => {
            const powerPlantDetail = { name: '1 Hammond St', code: '6487689', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            const powerplant = await request(app)
                .post("/api/power-plants")
                .send({ powerPlantDetail});
            expect(powerplant).not.toBeNull();
        });

        it('should return 400 if duplicate input passed', async () => {
            const powerPlantDetail = { name: '1 Hammond St', code: '6487689', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 };
            await PowerPlant.collection.insertMany([powerPlantDetail]);
            await request(app)
                .post("/api/power-plants")
                .send(powerPlantDetail)
                .expect(400);
        });

    });

    describe('DELETE Power plant by plant Id', () => {

        afterEach(async () => {
            await PowerPlant.deleteMany({});
        });

        const execDelete = (plantId: any) => {
            return request(app).delete('/api/power-plants/' + plantId);
        };


        it('should return 404 if plant id is not valid', async () => {
            const plantId = 'e7fd8862-a77d-46d6-945e-747eb8ee7a27';
            const res = await execDelete(plantId);
            expect(res.status).toBe(404);
        });

        it('should delete a power plant if valid input is passed', async () => {
            const powerPlantDetail = { name: '1 Hammond St', code: '6487689', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            const powerplant: any = await request(app).post("/api/power-plants").send(powerPlantDetail);
            await execDelete(powerplant.plantId).expect(200);
        });
    });

    describe('GET Power Plant by Plant Id', () => {

        afterEach(async () => {
            await PowerPlant.deleteMany({});
        });

        const execGet = (plantId: any) => {
            return request(app).get('/api/power-plants/' + plantId);
        };

        it('should return 404 if plant id is not valid', async () => {
            const plantId = 'e7fd8862-a77d-46d6-945e-747eb8ee7a27';
            const res = await execGet(plantId);
            expect(res.status).toBe(404);
        });

        it('should get a power plant if valid input is passed', async () => {
            const powerPlantDetail = { name: '0 Hammond St', code: '64876', loc: { lat: 41.808547, lng: -70.726675 }, stateCode: 'MA', year: 2020, genntan: 10, genntoz: 0 }
            const powerplant: any = await request(app).post("/api/power-plants").send(powerPlantDetail);
            await execGet(powerplant.plantId).expect(200);
        });
    });
});

