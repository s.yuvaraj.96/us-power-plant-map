import app from '../src/app';
import request from 'supertest';



describe("GET /health", () => {
    it('should get success', async () => {
      await request(app).get("/health").expect(200);
    });
});
