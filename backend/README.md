
## The US PowerPlant Map backend app

A backend application to get the statistical data of US power generation by power plants

## Prerequisites

* Node >= 16
* Git
* Mongo DB >= 4.2


## Common setup

Clone the project  us-power-plant-map-backend and install the dependencies using below command.

```bash
npm install
```

## Steps for access
Step 1: Open `.env` and inject your credentials so it looks like this

```
NODE_ENV=development
PORT=3000
DB_CONNECTION=mongodb://admin:admin@localhost:27017/us-power-plant?authSource=admin
```

To start the development express server, run the following

```bash
npm run start:dev
```

To build the project , run the following
```bash
npm run build
```

Navigate to http://localhost:3000/health, The API will return 200 OK

To load the data from excel, hit the following API,

http://localhost:3000/api/load-data

## Use Docker
You can also run this app as a Docker container:

Step 1: Clone the repo us-power-plant-map-backend

Step 2: Build the Docker image

```bash
docker build -t  . <image_name>
```

Step 3: Run the Docker container locally:

```bash
docker run -p 3000:3000 -d <image_name>
```

#Project API Details

- Exposed APIs to GET/PUT/POST/DELETE states data

- Exposed APIs to GET/PUT/POST/DELETE power plant data

- Exposed APIS to GET top N, State wise and plant wise data

- Visualize Map using HighCharts

#End Results

 Please check following for end results
 
 - Top N power plants display in Charts
 
    ![alt text](../sample-results/US_TopN_in_Charts.png)
 
  - Top N power plants display in Maps
  
    ![alt text](../sample-results/US_TopN_in_Maps.png)
  
 - State wise data US Map 
 
    ![alt text](../sample-results/US_State_Wise_Map.png)
 
 - Filtered State Power Plant data Map 
 
    ![alt text](../sample-results/US_State_Wise_Power_Plant.png)
 

## Cloud Deployment
 
 Due to time constraint unable to complete it

 - Convert app to lamda 
      ```
      // lambda.ts
      import * as awsServerlessExpress from 'aws-serverless-express';
      import app from './app';

      const appServerLess = awsServerlessExpress.configure({
      app,
      respondWithErrors: process.env.NODE_ENV !== 'production',
      loggerConfig: {
         level: 'debug'
      }
      });

      export default appServerLess;
      ```
 
 - Create serverless.yaml
   ```
   service: PowerPlantBackEndApp
   provider:
   name: aws
   memorySize: 256
   environment:
      DB_CONNECTION=mongodb://admin:admin@localhost:27017/us-power-plant?authSource=admin

   functions:
   express:
      handler: lambda.handler
      events:
      - http:
         method: ANY
         path: /
         cors: true
      - http:
         method: ANY 
         path: '{proxy+}'
         cors: true
      ```
- run npx sls deploy      
